#!/usr/local/bin/node

const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });

  // simulate longer waiting
  setTimeout(() => {
    ws.send(JSON.stringify({ type: "connected" }));
  }, Math.random() * 3500);
});