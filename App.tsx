import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { GameConnection } from "./containers/GameConnection";
import { Provider } from "react-redux";
import { store } from "./store";

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <GameConnection render={() => {
            return <Text>Game is connected</Text>
          }} />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fcfcfc',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
