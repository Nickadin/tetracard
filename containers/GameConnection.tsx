import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import LottieView from "lottie-react-native";
import { gameEventReceived } from "../modules/game/actions";

const url = "ws://localhost:8080/socket";

interface Props {
  render: () => any,
  gameEventReceived: (event: DecodedMessage) => void
}

interface State {
  isConnected: boolean
}

interface Message {
  data: string
}

interface DecodedMessage {
  type: string
}

class Connection extends Component<Props, State> {
  socket: WebSocket | null

  state = {
    isConnected: false
  };

  constructor(props: Props) {
    super(props);
    this.socket = null;
  }

  componentDidMount() {
    const socket = new WebSocket(url);
    socket.onmessage = this.handleParsedMessage
    this.socket = socket;
  }

  handleParsedMessage = (message: Message) => {
    try {
      const msg = JSON.parse(message.data)
      this.handleMessage(msg);
    } catch(e) {
      console.error(e.message)
     }
  };

  handleMessage = (message: DecodedMessage) => {
    switch(message.type) {
      case "connected":
        this.setState({ isConnected: true });
      default:
        this.props.gameEventReceived(message);
    }
  };

  render() {
    const { render } = this.props;
    const { isConnected } = this.state;

    return isConnected ? render() : (
      <View style={styles.container}>
        <Text style={styles.loadingText}>Connecting to a game..</Text>
        <LottieView
          source={require("../assets/bouncing_ball.json")}
          autoPlay
          loop
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fcfcfc',
    width: "100%",
    height: "100%"
  },
  loadingText: {
    alignSelf: "center",
    marginTop: "45%"
  }
});

const mapStateToProps = () => ({});

const mapDispatchToProps = { gameEventReceived };

export const GameConnection = connect(mapStateToProps, mapDispatchToProps)(Connection);