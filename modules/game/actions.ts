export const GAME_EVENT_RECEIVED = "GAME_EVENT_RECEIVED";

// todo figure out format
export const gameEventReceived = (event: any) => ({
  type: GAME_EVENT_RECEIVED,
  event
});
