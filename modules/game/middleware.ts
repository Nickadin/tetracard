import { Middleware } from "redux";
import { GAME_EVENT_RECEIVED } from './actions';

interface Action {
  type: string,
  event?: any
}

const handleGameEventMiddleware: Middleware = store => next => (action: Action) => {
  if (action.type !== GAME_EVENT_RECEIVED) {
    next(action);
  }

  // TODO
  next(action);
};

export const gameMiddlewares = [handleGameEventMiddleware];