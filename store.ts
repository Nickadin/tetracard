import { createStore, applyMiddleware } from "redux";
import { rootReducer } from './reducer';
import { gameMiddlewares } from './modules/game/middleware';

const middlewares = applyMiddleware(...gameMiddlewares);
export const store = createStore(rootReducer, middlewares);
